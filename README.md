# 산업기반 개념설계 연구회

- [ ] 
**목적**
 > 산업기반 논리체계를 객관화하기 위하여 기술-제품-산업과 연계한 산업기반 로드맵 XML 문서화 작업을 통해, 산업기술R&D 투자전략과 산업기술혁신계획 기반조성사업과의 연계성 논리를 마련하고자 합니다. 
 > 논리모형과 연계된 산업기반 로드맵을 바탕으로 체계적인 산업기술기반을 구축함으로써 산업기술기반구축사업의  인프라 유사⸱중복투자 방지와 활용성을 개선하고자 합니다.

- [ ] 
**내용**
 > 산업기반 개념설계를 기반으로 하여 테마를 도출하기 위한 논리모델템플릿 작성(논리의 흐름 적절성, 항목도출의 적절성, 항목간 논리에 대한 분석)등을 검토한다.

- [ ] 
**조직**
 > 논리그룹, 프로그램그룹, 툴 그룹으로 구성한다.

- [ ] 
**기간**
 > 2021년 12월까지 한시적으로 운영하고자 합니다.
