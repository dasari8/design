#---------------------------------------------------------------------
# This code makes hwpx(roadmap) from hwpx(model*)
# concept: input -> logic knowledge -> output
#
# usage:  python make_model2roadmap  <input model directory> <output_file>
#          python make_model2roadmap ./logic_model.hwpx ./roadmap.hwpx
#
# input dir  : logic_models directory
# output file : hwpx_roadmap
# logic knowledge : 
#
# copyright 2021.08 KIAT,  all right reserved.
#---------------------------------------------------------------------
#
# HISTORY
# 2021.08.16 make ver 1.0 (*.hwpx - domain knowledge (string replace) -> *.hwpx
# 2021.08.19 add multi file in folder  
# 2021.08.19 add weight factors
# 2021.08.20 add tkinter
#---------------------------------------------------------------------

import os
import re
import sys
import numpy as np
import zipfile
from   datetime       import datetime
from   pypreprocessor import pypreprocessor

# imports
import tkinter as tk
import pickle
from   tkinter.messagebox import showinfo,showerror,showwarning,askyesno
from   tkinter            import font

DEBUG = False #True
#---------------------------------------------------------------
def makeModel2Roadmap(model_folder_name, output_file_name):
   if DEBUG:
      print('makeModel2Roadmap debug mode ')

   #read arguments
   model_folder    = model_folder_name # './logic_models'  # multi logic model
   output_file     = output_file_name # './210812_HWPX_1_output.hwpx'

   logic_extracted = './_hwpx_logic'   # extracted logic model
   section_file    = 'Contents/section0.xml'

   template        = './logic_template'
   input_roadmap   = template + '/roadmap_template.hwpx'
   roadmap_folder  = './_hwpx_roadmap'

   #...............................................................
   def getFileList(dirname, result = None):
      if result is None:
         result = []
      try:
         filenames = os.listdir(dirname)
         for filename in filenames:
            full_filename = os.path.join(dirname, filename)
            if os.path.isdir(full_filename):
               getFileList(full_filename)
            else:
               ext = os.path.splitext(full_filename)[-1]
               if ext == '.hwpx': 
#                  print(full_filename)
                  result.append(full_filename)
      except PermissionError:
         pass
      return result

   #...............................................................
   #check model folder
   if os.path.isdir(model_folder):
      print(f'[S1] load {model_folder} and check logic model file')
   else:
      print(f'model folder:{model_folder} does not exist.')
      sys.exit()

   fileList  = getFileList(model_folder)
   logicNum  = len(fileList)  # logic file number
   if logicNum < 1:
      print(f'[S1] no model files in model folder {model_folder}')
      sys.exit()

   #read multi model file and check hwpx
   for iNum in range(len(fileList)):
      if DEBUG:
         print(" %2d %s" %(iNum, fileList[iNum]))
   
      input_file = fileList[iNum]  # logid model file in model folder
      ext_file     = os.path.splitext(input_file)[1][1:].strip().lower() #check file extension
      if ext_file != 'hwpx':
         print(f'file format: {input_file} is not supported.')
         sys.exit()
	  
   #...............................................................
   #check roadmap templage   
   if os.path.isfile(input_roadmap):
      print(f'[S2] read roadmap template {input_roadmap} file')
   else:
      print(f'roadmap:{input_roadmap} does not exist.')
      sys.exit()

   #...............................................................
   #extract from HWPX to HWPX folder and list files.
   print(f'[S3] extract files to {logic_extracted} folder')
   for iNum in range(logicNum):
      input_file       = fileList[iNum]
      logic_folder_num = logic_extracted + '/' + str(iNum)
      hwpx_logic       = zipfile.ZipFile(input_file)
      hwpx_logic.extractall(logic_folder_num)
      hwpx_logic.close()

      steps = 1
      if DEBUG:
         print(f' D{iNum}  list {input_file} files in {logic_folder_num} folder')
  
      for root, dirs, files in os.walk(logic_folder_num):
        for filename in files:
            if DEBUG:	  
                print(" %2d %s/%s" %(steps, root, filename))
            steps = steps + 1

   print(f'[S4] extract roadmap templage to {roadmap_folder} folder')
   hwpx_roadmap = zipfile.ZipFile(input_roadmap)
   hwpx_roadmap.extractall(roadmap_folder)
   hwpx_roadmap.close()

   #...............................................................
   #load domain knowledge, theme from input files and update & overwrite .
   #find A1 keyword and extract A1.***** string
   #A0:Theme, A4: output value 1, B4: output value 2, C4: weight factor 1, D4: weight factor2
   print(f'[S5] read logic model #{logicNum} from {logic_extracted} model folder')
   A457_txt        = ['TITLE','A4','B4','C4','D4','A5','B5','C5','D5','A7','B7','C7','D7']
   AD_elements    = []
   v_m            = []
   value_logicNum = np.zeros(logicNum) # (A4 B4 C4 D4),  (A4 B4 C4 D4), logicNum
   value_weightA  = np.zeros(logicNum) # (A4 B4 C4 D4),  (A4 B4 C4 D4), logicNum
   value_weightB  = np.zeros(logicNum) # (A4 B4 C4 D4),  (A4 B4 C4 D4), logicNum
   value_weightC  = np.zeros(logicNum) # (A4 B4 C4 D4),  (A4 B4 C4 D4), logicNum
   value_weightD  = np.zeros(logicNum) # (A4 B4 C4 D4),  (A4 B4 C4 D4), logicNum

   #...............................................................
   #read TITLE, A4:input, A5:activity, A7:output
   for iNum in range(logicNum):
      logic_model_dir = logic_extracted + '/' + str(iNum)
      model_file      = f'{logic_model_dir}/{section_file}'
      if DEBUG:   
         print(f' D{iNum}  model {model_file} file in {logic_model_dir} folder')

      model_section   = open(model_file, 'rt', encoding='UTF8')
      model_read      = model_section.read()
      model_section.close()       
      value_logicNum [iNum] = iNum  # A4_output_01   
      for idx in range (13):
         c_text    = A457_txt[idx]
         c_max     = 100
         c_target  = '<'  # </p..., XML
         c_index   = -1

         AD_index  = model_read.find(c_text)
         if AD_index == -1:
            if c_text=='A4' or c_text=='B4' or c_text=='C4' or c_text=='D4' : #output, weight value
               AD_newStr = '0'
            else:
               AD_newStr = 'undefined'
            if DEBUG:			
               print(" %2d  logic model code(%s): %s" %(idx, c_text, AD_newStr))
            AD_elements.append(AD_newStr)
			
         else: 
            AD_oldStr = model_read[AD_index: AD_index + 100]            # max string
            c_index   = AD_oldStr.find(c_target, c_index + 1)
            if c_index == -1:
               AD_newStr    = AD_oldStr                                 # old.. 
            else:
               AD_newStr    = model_read[AD_index+len(c_text)+1: AD_index + c_index] # skip 'AD.' new string
            if DEBUG:
               print(" %2d logic model code(%s,%2d): %s" %(idx, c_text, c_index, AD_newStr))

            if c_text=='A4' or c_text=='B4' or c_text=='C4' or c_text=='D4' : 
               v_m      = [float(v_m) for v_m in re.findall(r'-?\d+\.?\d*', AD_newStr)]
               valFloat =  float(v_m[0])
               if DEBUG:
                  print(" A4-D4: %3.2f" %(valFloat))	

               if c_text=='A4':
                  value_weightA[iNum] = valFloat  # A4_output_01
               elif c_text=='B4':
                  value_weightB[iNum] = valFloat  # B4_output_02
               elif c_text=='C4':
                  value_weightC[iNum] = valFloat  # C4_weight_01
               else:
                  value_weightD[iNum] = valFloat  # D4_weight_02	 	               
		    #rearrange value from string, ex) D4.0.5 ****  -> 0.5
            #if c_text=='A4' or c_text=='B4' or c_text=='C4' or c_text=='D4' : #output, weight value
		    # 	#value_weight = [float(value_weight) for value_weight in re.findall(r'-?\d+\.?\d*', AD_newStr)]
		    #    print(" A4-D4: %6.2f" %(value_weight))	
            AD_elements.append(AD_newStr)

   #rearrange theme order from weight factor2 value
   theme2D    = np.array( (value_logicNum, value_weightA, value_weightB, value_weightC, value_weightD), dtype=float )
   themeIdx   = theme2D[4].argsort()[::-1][:logicNum]     # 4: weight factor2 D4
   orderTheme = theme2D [ :, themeIdx]                    # theme2D[4].argsort()]
   if DEBUG:
      print('theme weight:', theme2D)
      print('weight reorrderd:', orderTheme)

   #...............................................................
   #read A6, B6, C6, D6 from logic model each
   A6_txt      = ['A6','B6','C6','D6']
   A6_elements = []
   for iNum in range(logicNum):
      logic_model_dir = logic_extracted + '/' + str(iNum)
      model_file      = f'{logic_model_dir}/{section_file}'
      if DEBUG:   
         print(f' D{iNum}  model {model_file} file in {logic_model_dir} folder')

      model_section   = open(model_file, 'rt', encoding='UTF8')
      model_read      = model_section.read()
      model_section.close()  
	  
      for idx in range (4):         # A6, B6, C6, D6,  activity
         for i_year in range (5):   # year
            c_text    = A6_txt[idx] + '-' + str(i_year+1)
            c_max     = 100
            c_target  = '<'  # </p..., XML
            c_index   = -1

            A6_index  = model_read.find(c_text)
            #print('a6:', A6_index) 
            if A6_index == -1:
               A6_newStr = '0'
            else:
               A6_oldStr = model_read[A6_index: A6_index + 100]            # max string
               c_index   = A6_oldStr.find(c_target, c_index + 1)
               if c_index == -1:
                  A6_newStr    = A6_oldStr                                 # old.. 
               else:
                  A6_newStr    = model_read[A6_index+len(c_text)+1: A6_index + c_index] # skip 'AD.' new string
               if DEBUG:
                  print(" %2d logic model code(%s,%2d): %s" %(idx, c_text, c_index, A6_newStr))

            A6_elements.append(A6_newStr)

   #A6_element 4 row(subject) x 5 column(year) in each theme, '0'=undefined.  		 
   if DEBUG:
      print(" A6(%d):%s" %(len(A6_elements), A6_elements)) # inum * 20
   
   #...............................................................
   # rearrange A6 from A6_elements
   A6_reOrder = A6_elements[:]
   for iNum in range(logicNum):
      orderS = int(orderTheme[0,iNum] * 20)  #20 = 4x5
      #no tech move
      if iNum==0:
         for i_act in range (4):                           # A6, B6, C6, D6,  activity
            for i_year in range (5):                       # year
               i_act_year                       = i_act*5 + i_year
               A6_reOrder[i_act_year + iNum*20] = A6_elements[i_act_year + orderS]
      #1 year move 
      elif iNum==1:
         for i_act in range (4):                           # A6, B6, C6, D6,  activity
            i_act_year                          = i_act*5
            A6_reOrder[i_act_year + iNum*20]    = '0'
         for i_act in range (4):                           # A6, B6, C6, D6,  activity
            for i_year in range (4):                      
               i_old_year                       = i_act*5 + i_year
               i_new_year                       = i_act*5 + i_year + 1 #1-year
               A6_reOrder[i_new_year + iNum*20] = A6_elements[i_old_year + orderS]
               #print(" A6_elements(%d)-> A6_reOrder(%d):%s" %(i_old_year + orderS, i_new_year + iNum*20, A6_reOrder[i_new_year + iNum*20] )) 
      #2 year move
      elif iNum==2:
         for i_act in range (4):                           # A6, B6, C6, D6,  activity
            i_act_year                          = i_act*5
            A6_reOrder[i_act_year + iNum*20]    = '0'
            i_act_year                          = i_act*5 + 1
            A6_reOrder[i_act_year + iNum*20]    = '0'
         for i_act in range (4):                           # A6, B6, C6, D6,  activity
            for i_year in range (3):                       
               i_old_year                       = i_act*5 + i_year
               i_new_year                       = i_act*5 + i_year + 2  #2-year
               A6_reOrder[i_new_year + iNum*20] = A6_elements[i_old_year + orderS]
               #print(" A6_elements(%d)-> A6_reOrder(%d):%s" %(i_old_year + orderS, i_new_year + iNum*20, A6_reOrder[i_new_year + iNum*20] )) 
      else:
         for i_act in range (4):                           # A6, B6, C6, D6,  activity
            for i_year in range (5):                       # year
               i_act_year                       = i_act*5 + i_year
               A6_reOrder[i_act_year + iNum*20] = A6_elements[i_act_year + orderS]	 
			   
   if DEBUG:
      print(" A6_reOrder(%d):%s" %(len(A6_reOrder), A6_reOrder[0:19])  ) # inum * 20
      print(" A6_reOrder(%d):%s" %(len(A6_reOrder), A6_reOrder[20:39]) ) # inum * 20
      print(" A6_reOrder(%d):%s" %(len(A6_reOrder), A6_reOrder[40:59]) ) # inum * 20
      
   #...............................................................
   #update domain knowledge, theme from input files and update & overwrite .
   #print(f'[S3] read data and write domain knowledge')
   date_old  = '2000.01.01. 00:00:00'
   date_new  = datetime.today().strftime("%Y.%m.%d. %H:%M:%S")
   print('[S6] update ', date_new, 'roadmap from logic model')

   roadmap_file    = f'{roadmap_folder}/{section_file}'
   roadmap_section = open(roadmap_file, 'rt', encoding='UTF8')
   roadmap_save    = roadmap_section.read()
   roadmap_section.close()

   #replace TITLE, A4, A5, A7 
   for iNum in range(logicNum):
      orderS = int(orderTheme[0,iNum] * 13)
      if DEBUG:   
         print('order:', orderS)

      for idx in range (13):
         editTxt      = str(iNum)+'-'+ A457_txt[idx]	
         if DEBUG:
            print(" %2d %s->%s" %(idx, editTxt, AD_elements[orderS + idx]))
         if AD_elements[orderS + idx]=='0' or AD_elements[orderS + idx]=='undefined':
            roadmap_save = roadmap_save.replace(editTxt, ' ')
         else:
            roadmap_save = roadmap_save.replace(editTxt, AD_elements[orderS+idx])
	 
   #replace 0-1-A6, 
   print('[S7] replace tech roadmap from logic model') 
   A6_txt        = ['A6','B6','C6','D6']
   for iNum in range(logicNum):
      orderS = iNum*20
      for i_act in range (4):       # A6, B6, C6, D6,  activity
         for i_year in range (5):   # year
            reNew   = str(i_year+1)
            editTxt = str(iNum)+'-'+ reNew +'-'+ A6_txt[i_act]
            idx     = i_year + i_act*5 + orderS
            if A6_reOrder[idx] == '0' or A6_reOrder[idx] == 'undefined':
                A6_str = ' '
            else:
                A6_str = A6_reOrder[idx]
            roadmap_save = roadmap_save.replace(editTxt, A6_str)
            if DEBUG:
               print(" %2d %s->%s" %(idx, editTxt, A6_str)) 
   
            #roadmap_save = roadmap_save.replace(editTxt, A6_reOrder[idx]) 	  
   roadmap_save    = roadmap_save.replace(date_old,   date_new)
   roadmap_section = open(roadmap_file, 'wt', encoding='UTF8')
   roadmap_section.write(roadmap_save)            #update new string
   roadmap_section.close()

   #...............................................................
   #write folder to hwpx file
   print(f'[S8] write hwpx folder to {output_file} file')

   #strict_timestamps=True : ValueError: ZIP does not support timestamps before 1980
   hwpx_roadmap = zipfile.ZipFile(output_file, 'w', strict_timestamps=False)
   for folder, subfolders, files in os.walk(roadmap_folder):
     for file in files:
        hwpx_roadmap.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder,file), \
                        roadmap_folder), compress_type = zipfile.ZIP_DEFLATED)

   hwpx_roadmap.close()

   print('[S9] done')
   return 0


#.............................................................................
def logic2roadmap():
    logic_model = var_logic_model.get()
    roadmap     = var_roadmap.get()
	 
    ret = makeModel2Roadmap(logic_model, roadmap)
    if ret==0:
        showinfo("메세지", f' 기반구축 로드맵 파일이 생성되었습니다. \n {roadmap}')
    
def program_quit():
    window.destroy()
      

#-----------------------------------------------------------------------------
if __name__ == '__main__':	  
    window=tk.Tk()
    window.title('Domain Knowledge Python Code')
    window.geometry('600x400')

    canvas     = tk.Canvas(window,height=400,width=600)
    image_file = tk.PhotoImage(file='./setting/base.png')
    image      = canvas.create_image(0,0,anchor='nw',image=image_file)
    canvas.pack(side='top')
    canvas.create_line(0, 335, 599, 335, fill='dark gray')

    fonts=list(font.families())
    fonts.sort()
    #print(fonts)

    default_font = font.nametofont("TkDefaultFont")
    default_font.configure(size=12)
    #print(default_font) 

    #user information
    #tk.Label(window,text='Password:',font=('Microsoft elegant black, 20')).place(x=50,y=220)
    tk.Label(window,text='로직모델 폴더명', bg="white").place(x=50,y=210)
    tk.Label(window,text='출력 화일명', bg="white").place(x=50,y=260)

    var_logic_model=tk.StringVar()
    var_logic_model.set('logic_models')
    entry_logic_model=tk.Entry(window, width = 30, textvariable=var_logic_model)
    entry_logic_model.place(x=180,y=215)

    var_roadmap=tk.StringVar()
    var_roadmap.set('logic_roadmap/roadmap.hwpx')
    entry_roadmap=tk.Entry(window, width = 30,textvariable=var_roadmap)
    entry_roadmap.place(x=180,y=265)
   
    btn_logic2roadmap = tk.Button(window,text='  실 행  ',height=4,width=10, command=logic2roadmap)
    btn_logic2roadmap.place(x=410,y=200)

    btn_program_quit = tk.Button(window,text=' 종 료 ', height=1,width=5,command=program_quit)
    btn_program_quit.place(x=520,y=30)

    #default_font.configure(size=8)
    tk.Label(window,text='설명: 로직모델 폴더에 있는 논리모형 파일(*.hwpx)들을 읽어서', bg="white").place(x=30,y=345)
    tk.Label(window,text='        기반구축 로드맵 우선순위를 정하여 파일을 자동으로 생성시킵니다.', bg="white").place(x=30,y=370)
    window.resizable(0,0)
    window.mainloop()

